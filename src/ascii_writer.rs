use core::result::Result::Err;

// Side-effects only
pub fn _get_graphical_display(_line: String) -> String {
    return "Just testing".to_string();
}

pub struct Display {
    #[allow(dead_code)]
    // ##### ##### ##### # ##### #####
    // 0     6     12   1719     26
    lines: [[bool; 30]; 4],
    slots: usize,
    rows: usize,
    columnns: usize
}

#[allow(dead_code)]
impl Display {
    pub fn new() -> Display {
        Display {
            lines: [[false; 30]; 4],
            slots: 5,
            rows: 5,
            columnns: 36
        }
    }

    fn fill_slot(&mut self, slot_number: usize, _n: usize) -> Result<(), &'static str> {
        if slot_number > self.slots {
            return Err("You used a slot that is higher than the number of slots available")
        }

        let col_number = Display::get_col_num(slot_number);

        return Ok(());
    }

    fn get_col_num(n: usize) -> usize {
        match {n} {
            1 => 0,
            2 => 6,
            3 => 12,
            4 => 19,
            5 => 26,
            _ => 0
        }
    }
}














fn _get_ascii_fun(e: usize) -> String {
    match { e } {
        0 =>
            "XXXXX
             X   X
             X   X
             X   X
             XXXXX"
            .to_string(),
        1 =>
            " XX
             X X
               X
             XXXXX"
            .to_string(),
        2 =>
            "XXXXX
                 X
             XXXXX
             X
             XXXXX"
            .to_string(),
        3 =>
            "XXXXX
                 X
             XXXXX
                 X
             XXXXX"
            .to_string(),
        4 =>
            "X   X
             X   X
             XXXXX
                 X
                 X"
        .to_string(),
        5 =>
            "XXXXX
             X
             XXXXX
                 X
             XXXXX"
            .to_string(),
        6 =>
            "XXXXX
             X
             XXXXX
             X   X
             XXXXX"
            .to_string(),
        7 =>
            "XXXXX
                 X
                X
               X
              X   "
            .to_string(),
        8 =>
            "XXXXX
             X   X
             XXXXX
             X   X
             XXXXX"
            .to_string(),
        9 =>
            "XXXXX
             X   X
             XXXXX
                 X
                 X"
        .to_string(),
        _ => "Not implemented yet".to_string(),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new_returns_correctly() {
        let result = Display::new();
        assert_eq!(result.lines, [[false; 30]; 4]);
    }

    #[test]
    fn get_col_num_works_1() {
        assert_eq!(Display::get_col_num(1), 0);
    }

    #[test]
    fn get_col_num_works_2() {
        assert_eq!(Display::get_col_num(2), 6);
    }

    #[test]
    fn get_col_num_works_3() {
        assert_eq!(Display::get_col_num(3), 12);
    }

    #[test]
    fn get_col_num_works_4() {
        assert_eq!(Display::get_col_num(4), 19);
    }

    #[test]
    fn get_col_num_works_5() {
        assert_eq!(Display::get_col_num(5), 26);
    }
}


















