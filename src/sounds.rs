use std::process::Command;
use std::thread;
use std::time::Duration;

enum Speed {
    SoFast,
    Fast,
    Slow,
}

pub enum SoundType {
    None,
    Bells,
    Quiet,
    Voice,
}

// Not a pure function, only side effect driven
pub fn done_sound(sound: SoundType) {
    match sound {
        SoundType::Bells => {
            done_bells(5, Speed::Slow);
            done_bells(5, Speed::Fast);
            done_bells(5, Speed::SoFast);
        },
        SoundType::None => {},
        SoundType::Quiet => play_quiet_sound_file(),
        SoundType::Voice => play_sound_file()
    }
}

// Not a pure function, only side effect driven
fn play_quiet_sound_file() {
    play_sound_file_wrapper("/home/brianbreniser/Recordings/times_up_quiet.mp3".to_string());
}

// Not a pure function, only side effect driven
fn play_sound_file() {
    play_sound_file_wrapper("/home/brianbreniser/Recordings/times_up.mp3".to_string());
}

fn play_sound_file_wrapper(file_location: String) {
    match check_dependencies() {
        Ok(_) => {
            Command::new("sh")
                .arg("-c")
                .arg(format!("/usr/bin/mplayer {}", file_location))
                .output() // Capture output even if we ignore it
                .expect("Failed to play sound file");
            ()
        },
        Err(x) => {
            eprintln!("failed to play sound file, error: {}", x);
            ()
        }
    }
}

fn check_dependencies() -> Result<(), String> {
    let command_result = Command::new("sh")
        .arg("-c")
        .arg("which mplayer")
        .output()
        .expect("mplayer command not found");

    if command_result.status.success() {
        Ok(())
    } else {
        eprintln!("command \"mplayer\" not found");
        return Err("command \"mplayer\" not found".to_string())
    }
}

// Not a pure function, has side effects
fn done_bells(count: i64, speed: Speed) {
    let bell = "\u{0007}";
    let erase_to_end = "\x1B[K";
    let move_cursor_to_previous_line = "\x1B[F";

    for _ in 0..count {
        match speed {
            Speed::Slow => thread::sleep(Duration::from_millis(500)),
            Speed::Fast => thread::sleep(Duration::from_millis(250)),
            Speed::SoFast => thread::sleep(Duration::from_millis(100)),
        }

        print!("{}{}", move_cursor_to_previous_line, erase_to_end);
        println!("{}", bell);
    }
}