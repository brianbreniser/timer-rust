use std::ops::Index;
use chrono::prelude::{DateTime, Local};
use chrono::{Duration as chronoDuration, Timelike};
use clap::{App, Arg};
use notify_rust::Notification;
use std::thread;
use std::time::Duration;

mod sounds;
mod ascii_writer;

use sounds::{done_sound, SoundType};

enum Text {
    Simple//,
    //Graphical
}

fn main() {
    // Set up our applications arguments
    let matches = App::new("Timer")
        .version("0.2")
        .author("Brian Breniser <git@breniser.me")
        .about("Set a countdown timer on the command line")
        .arg(
            Arg::with_name("sound")
                .help("Sets the sound type, none, bells, voice, or quiet")
                .required(false)
                .short("s")
                .long("sound")
                .value_name("sound"),
        )
        .arg(
            Arg::with_name("minutes")
                .help("Sets the number of minutes for the timer to run")
                .required(false)
                .short("m")
                .long("minutes")
                .value_name("minutes")
                .index(1),
        )
        .arg(
            Arg::with_name("title")
                .help("Sets the title to use for notification")
                .required(false)
                .short("t")
                .long("title")
                .value_name("title")
                .index(2)
                .multiple(true),
        )
        .get_matches();

    let minutes = matches.value_of("minutes").unwrap_or("0");
    let mut seconds = parse_seconds(minutes);

    // title to show user
    let mut title: String = "".to_string();
    if matches.is_present("title") {
        title = matches
            .values_of("title")
            .unwrap()
            .map(|x| append_space(x.to_string()))
            .collect();
    }

    // Get sound setting
    let sound = match matches.value_of("sound").unwrap_or("") {
        "none" => SoundType::None,
        "bells" => SoundType::Bells,
        "quiet" => SoundType::Quiet,
        "voice" => SoundType::Voice,
        _ => SoundType::None,
    };

    let final_title = title.trim();

    let end = Local::now() + chronoDuration::seconds(seconds);

    // Side effect
    // starting timer notification
    let body = format!(
    "your timer is starting for: {}, and will get done at: {:?}",
        get_time(seconds),
        format_chrono_time(end)
    );
    let end_body = format!("timer of {} is done", get_time(seconds));
    let start_body: &str = &body;
    let end_body_reference: &str = &end_body;

    // side effect
    Notification::new().summary(final_title).body(start_body).show().unwrap();

    // Side effect
    println!("Counting down: {}", get_time(seconds));

    // Side effect
    // Start our countdown timer
    while seconds > 0 {
        thread::sleep(Duration::from_secs(1));

        seconds = seconds - 1;

        // TODO: My Rust foo is old, how do I get around clone here? Does it matter?
        output(seconds.clone(), end.clone());
    }

    // Side effect
    done_sound(sound);

    // Side effect
    let _endres = Notification::new().summary(final_title).body(end_body_reference).show_debug();
}

fn parse_seconds(minutes: &str) -> i64 {
    return if minutes.contains(".") {
        (minutes.parse::<f64>().unwrap_or(0.0f64) * 60_f64) as i64
    } else if minutes.contains(":") {
        let mut seconds = 0i64;
        let m: Vec<&str> = minutes.split(":").collect();
        seconds += m.index(0).parse::<i64>().unwrap_or(0i64) * 60i64;
        seconds += m.index(1).parse::<i64>().unwrap_or(0i64);
        seconds
    } else {
        (minutes.parse::<f64>().unwrap_or(0.0f64) * 60_f64) as i64
    }
}

// Side effects for sure!
fn output(seconds: i64, end: DateTime<Local>) {
    let erase_to_end = "\x1B[K";
    let move_cursor_to_previous_line = "\x1B[F";

    let output_mode = Text::Simple;

    match { output_mode } {
        Text::Simple => {
            print!("{}{}", move_cursor_to_previous_line, erase_to_end);
            println!(
                "t-: {}, due @: {:?}",
                get_time(seconds),
                format_chrono_time(end)
            );
        }//,
        // Text::Graphical => {
        //     println!("in graphical")
        // }
    }
}

fn format_chrono_time(time: DateTime<Local>) -> String {
    format!("{:02}:{:02}:{:02}", time.hour(), time.minute(), time.second())
}

// Pure function, but too simple to test
fn append_space(x: String) -> String {
    format!("{} ", x)
}

// Pure function bitches
fn get_time(seconds: i64) -> String {
    let buff_m = if ((seconds / 60) as i64) < 10 {
        "0"
    } else {
        ""
    };
    let buff_s = if (seconds % 60) < 10 { "0" } else { "" };

    return format!("{}{}:{}{}", buff_m, seconds / 60, buff_s, seconds % 60);
}

//
// Tests
//

#[cfg(test)]
mod tests {
    use super::*;
    // use chrono::Date;

    //
    // Testing clock display function
    //

    #[test]
    fn get_time_works_0_seconds() {
        assert_eq!("00:00", get_time(0));
    }

    #[test]
    fn get_time_works_1_seconds() {
        assert_eq!("00:01", get_time(1));
    }

    #[test]
    fn get_time_works_2_seconds() {
        assert_eq!("00:02", get_time(2));
    }

    #[test]
    fn get_time_works_60_seconds() {
        assert_eq!("01:00", get_time(60));
    }

    #[test]
    fn get_time_works_61_seconds() {
        assert_eq!("01:01", get_time(61));
    }

    #[test]
    fn get_time_works_120_seconds() {
        assert_eq!("02:00", get_time(120));
    }

    #[test]
    fn get_time_works_3599_seconds() {
        assert_eq!("59:59", get_time(3599));
    }

    #[test]
    fn get_time_works_3600_seconds() {
        assert_eq!("60:00", get_time(3600));
    }

    #[test]
    fn get_time_works_5999_seconds() {
        assert_eq!("99:59", get_time(5999));
    }

    #[test]
    fn get_time_works_6000_seconds() {
        assert_eq!("100:00", get_time(6000));
    }

    #[test]
    fn get_time_works_60000_seconds() {
        assert_eq!("1000:00", get_time(60000));
    }

    #[test]
    fn parse_seconds_parses_0() {
        let seconds = parse_seconds("0");
        assert_eq!(seconds, 0);
    }

    #[test]
    fn parse_seconds_parses_1() {
        let seconds = parse_seconds("1");
        assert_eq!(seconds, 60);
    }

    #[test]
    fn parse_seconds_parses_2() {
        let seconds = parse_seconds("2");
        assert_eq!(seconds, 120);
    }

    #[test]
    fn parse_seconds_parses_2dot0() {
        let seconds = parse_seconds("2.0");
        assert_eq!(seconds, 120);
    }

    #[test]
    fn parse_seconds_parses_2colon0() {
        let seconds = parse_seconds("2:0");
        assert_eq!(seconds, 120);
    }

    #[test]
    fn parse_seconds_parses_0colon15() {
        let seconds = parse_seconds("0:15");
        assert_eq!(seconds, 15);
    }

    #[test]
    fn parse_seconds_parses_1colon15() {
        let seconds = parse_seconds("1:15");
        assert_eq!(seconds, 75);
    }

    #[test]
    fn parse_seconds_parses_1colon() {
        let seconds = parse_seconds("1:");
        assert_eq!(seconds, 60);
    }

    #[test]
    fn parse_seconds_parses_colon15() {
        let seconds = parse_seconds(":15");
        assert_eq!(seconds, 15);
    }
}
